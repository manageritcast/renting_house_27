import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from 'react-router-dom'

// 导入semantic样式
import 'semantic-ui-css/semantic.min.css'

// 导入组件
import Main from './components/main/Main'
import Login from './components/login/Login'
import List from './components/home/List'
import NotFound from './components/other/NotFound'

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/main" component={Main} />
          {/* Redirect 必须写在已有路径的后面 */}
          <Redirect exact from="/" to="/login" />
          <Route path="/login" component={Login} />
          <Route path="/list" component={List} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    )
  }
}

export default App
