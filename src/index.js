import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'

// 导入axios
import axios from 'axios'
axios.defaults.baseURL = 'http://47.96.21.88:8086/'

// 添加请求拦截器
axios.interceptors.request.use(
  function(config) {
    // 在发送请求之前做些什么
    if (localStorage.getItem('mytoken')) {
      config.headers.Authorization = localStorage.getItem('mytoken')
    }
    return config
  },
  function(error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 响应拦截器
axios.interceptors.response.use(
  function(response) {
    // 对响应数据做点什么
    return response.data
  },
  function(error) {
    // 对响应错误做点什么
    return Promise.reject(error)
  }
)

ReactDOM.render(<App />, document.getElementById('root'))
