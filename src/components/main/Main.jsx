import React from 'react'
import { Grid, Icon } from 'semantic-ui-react'
import { Route, Link } from 'react-router-dom'

//导入样式
import './Main.css'

//导入组件
import Home from '../home/Home'
import Info from '../info/Info'
import Chat from '../chat/Chat'
import My from '../my/My'

class Menu extends React.Component {
  render() {
    // 解构赋值
    const { to, exact, menuName, iconName } = this.props

    return (
      <Route
        path={to}
        exact={exact}
        children={({ match }) => (
          <Link to={to}>
            <div className={`placeholder ${match ? 'active' : ''}`}>
              <Icon name={iconName} />
              <div>{menuName}</div>
            </div>
          </Link>
        )}
      />
    )
  }
}

export default class Main extends React.Component {
  render() {
    return (
      <div>
        {/* 1、内容显示区域 */}
        <div className="main-content">
          <Route exact path="/main" component={Home} />
          <Route path="/main/info" component={Info} />
          <Route path="/main/chat" component={Chat} />
          <Route path="/main/my" component={My} />
        </div>
        {/* 2、底部导航条 */}
        <div className="main-menu">
          <Grid centered padded>
            {/* 一行显示4列 */}
            <Grid.Row columns={4} divided>
              <Grid.Column>
                <Menu
                  to="/main"
                  exact={true}
                  menuName="主页"
                  iconName="user secret"
                />
              </Grid.Column>
              <Grid.Column>
                <Menu
                  to="/main/info"
                  menuName="咨询"
                  iconName="window restore"
                />
              </Grid.Column>
              <Grid.Column>
                <Menu to="/main/chat" menuName="微聊" iconName="microchip" />
              </Grid.Column>
              <Grid.Column>
                <Menu
                  to="/main/my"
                  menuName="我的"
                  iconName="window maximize"
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </div>
    )
  }
}
