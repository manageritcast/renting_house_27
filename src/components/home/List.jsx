import React from 'react'
import axios from 'axios'
import { Icon, Item } from 'semantic-ui-react'

export default class List extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      list: [],
      menuName: ''
    }
  }

  async componentWillMount() {
    const { home_type, menuName } = this.props.history.location.state

    // 赋值给模型
    this.setState({
      menuName
    })

    // 发送网络请求
    const res = await axios.post('homes/list', {
      home_type
    })

    this.setState({
      list: res.data
    })
  }

  render() {
    const { menuName, list } = this.state
    return (
      <div className="house-list">
        {/* 导航栏目 */}
        <div className="house-list-title">
          <Icon
            onClick={() => {
              this.props.history.goBack()
            }}
            name="angle left"
            size="large"
          />
          {menuName}
        </div>
        {/* 房屋列表 */}
        <div className="house-list-content">
          <Item.Group divided unstackable>
            {list.map(item => {
              return (
                <Item key={item.id}>
                  <Item.Image
                    size="tiny"
                    src="http://47.96.21.88:8086/public/home.png"
                  />
                  <Item.Content>
                    <Item.Header>{item.home_name}</Item.Header>
                    <Item.Meta>{item.home_desc}</Item.Meta>
                    <Item.Description>{item.home_tags}</Item.Description>
                    <Item.Description>{item.home_price}</Item.Description>
                  </Item.Content>
                </Item>
              )
            })}
          </Item.Group>
        </div>
      </div>
    )
  }
}
