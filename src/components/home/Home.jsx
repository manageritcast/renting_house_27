import React from 'react'
import {
  Grid,
  Input,
  Icon,
  Item,
  Button,
  Dimmer,
  Loader
} from 'semantic-ui-react'
import ImageGallery from 'react-image-gallery'
import axios from 'axios'
import Calc from './Calculator'
import Map from './Map'

// 导入样式
import 'react-image-gallery/styles/css/image-gallery.css'
import './Home.css'

export default class Home extends React.Component {
  constructor() {
    super()

    this.state = {
      loading: true, // 正在加载中
      swipers: [], // 轮播图
      menus: [], // 菜单
      infos: [], //咨询
      faqs: [], //问答数据
      houses: [], //房屋列表
      isShowCalc: false, //是否显示计算器
      isShowMap: false //是否显示地图
    }
  }

  componentDidMount() {
    // 发送网络请求
    Promise.all([
      axios.post('homes/swipe'),
      axios.post('homes/menu'),
      axios.post('homes/info'),
      axios.post('homes/faq'),
      axios.post('homes/house')
    ]).then(results => {
      this.setState({
        swipers: results[0].data.list,
        menus: results[1].data.list,
        infos: results[2].data.list,
        faqs: results[3].data.list,
        houses: results[4].data.list,
        loading: false
      })
    })
  }

  // 点击了菜单按钮
  handleMenus = menuName => {
    switch (menuName) {
      case '二手房':
        this.props.history.push('/list', { menuName, home_type: 1 })
        break

      case '新房':
        this.props.history.push('/list', { menuName, home_type: 2 })
        break

      case '租房':
        this.props.history.push('/list', { menuName, home_type: 3 })
        break

      case '海外':
        this.props.history.push('/list', { menuName, home_type: 4 })
        break

      case '地图找房':
        this.setState({
          isShowMap: true
        })
        break

      case '计算器':
        this.setState({
          isShowCalc: true
        })
        break

      case '问答':
        break

      default:
        break
    }
  }

  render() {
    /**
     * 菜单组件的内容
     */
    const Menus = props => {
      return (
        <Grid padded>
          {/* 一行四列 */}
          <Grid.Row columns={4}>
            {props.menus.map(item => {
              return (
                <Grid.Column
                  onClick={() => {
                    this.handleMenus(item.menu_name)
                  }}
                  key={item.id}
                >
                  {/* 图标 */}
                  <div className="home-menu-item">
                    <Icon name="home" size="big" />
                  </div>
                  {/* 文字 */}
                  <div style={{ marginTop: 5 }}>{item.menu_name}</div>
                </Grid.Column>
              )
            })}
          </Grid.Row>
        </Grid>
      )
    }

    /**
     * 咨询
     */
    const Infos = ({ infos }) => {
      return (
        <div className="home-msg">
          <Item.Group unstackable>
            <Item className="home-msg-img">
              <Item.Image
                size="tiny"
                src="http://47.96.21.88:8086/public/zixun.png"
              />
              <Item.Content>
                {/* 生成多行 */}
                {infos.map(item => {
                  return (
                    <Item.Header key={item.id}>
                      <span>限购 ●</span>
                      <span>{item.info_title}</span>
                    </Item.Header>
                  )
                })}
                <div className="home-msg-more">
                  <Icon name="angle right" size="big" />
                </div>
              </Item.Content>
            </Item>
          </Item.Group>
        </div>
      )
    }

    // 问答
    const Faqs = ({ faqs }) => {
      return (
        <div className="home-ask">
          <div className="home-ask-title">好客问答</div>
          <ul>
            {faqs.map(item => {
              return (
                <li key={item.question_id}>
                  {/* 标题 */}
                  <div>
                    <Icon name="question circle outline" />
                    <span>{item.question_name}</span>
                  </div>
                  {/* 标签 */}
                  <div>
                    {item.question_tag.split(',').map((item, index) => {
                      return (
                        <Button
                          key={index}
                          basic
                          color="green"
                          size="mini"
                          content={item}
                        />
                      )
                    })}
                    <div style={{ marginBottom: 5 }}>
                      {item.atime} ● <Icon name="comment alternate outline" />
                      {item.qnum}
                    </div>
                  </div>
                </li>
              )
            })}
          </ul>
        </div>
      )
    }

    /**
     * 房屋列表
     */
    const Houses = ({ houses }) => {
      // 最新开盘
      const newHouses = houses.filter(item => item.home_type === 1)
      // 二手精选
      const oldHouses = houses.filter(item => item.home_type === 2)
      // 热门房源
      const hotHouses = houses.filter(item => item.home_type === 3)
      return (
        <div>
          <div>
            <div className="home-hire-title">最新开盘</div>
            <Item.Group divided unstackable>
              {// 最新开盘每一项
              newHouses.map(item => {
                return (
                  <Item key={item.id}>
                    <Item.Image
                      src={'http://47.96.21.88:8086/public/home.png'}
                    />
                    <Item.Content>
                      <Item.Header>{item.home_name}</Item.Header>
                      <Item.Meta>
                        <span className="cinema">{item.home_desc}</span>
                      </Item.Meta>
                      <Item.Description>
                        {item.home_tags.split(',').map((subitem, index) => {
                          return (
                            <Button
                              key={index}
                              basic
                              color="green"
                              size="mini"
                              content={subitem}
                            />
                          )
                        })}
                      </Item.Description>
                      <Item.Description>{item.home_price}</Item.Description>
                    </Item.Content>
                  </Item>
                )
              })}
            </Item.Group>
          </div>
          <div>
            <div className="home-hire-title">二手精选</div>
            <Item.Group divided unstackable>
              {// 二手精选每一项
              oldHouses.map(item => {
                return (
                  <Item key={item.id}>
                    <Item.Image
                      src={'http://47.96.21.88:8086/public/home.png'}
                    />
                    <Item.Content>
                      <Item.Header>{item.home_name}</Item.Header>
                      <Item.Meta>
                        <span className="cinema">{item.home_desc}</span>
                      </Item.Meta>
                      <Item.Description>
                        {item.home_tags.split(',').map((subitem, index) => {
                          return (
                            <Button
                              key={index}
                              basic
                              color="green"
                              size="mini"
                              content={subitem}
                            />
                          )
                        })}
                      </Item.Description>
                      <Item.Description>{item.home_price}</Item.Description>
                    </Item.Content>
                  </Item>
                )
              })}
            </Item.Group>
          </div>
          <div>
            <div className="home-hire-title">热门房源</div>
            <Item.Group divided unstackable>
              {// 热门房源每一项
              hotHouses.map(item => {
                return (
                  <Item key={item.id}>
                    <Item.Image
                      src={'http://47.96.21.88:8086/public/home.png'}
                    />
                    <Item.Content>
                      <Item.Header>{item.home_name}</Item.Header>
                      <Item.Meta>
                        <span className="cinema">{item.home_desc}</span>
                      </Item.Meta>
                      <Item.Description>
                        {item.home_tags.split(',').map((subitem, index) => {
                          return (
                            <Button
                              key={index}
                              basic
                              color="green"
                              size="mini"
                              content={subitem}
                            />
                          )
                        })}
                      </Item.Description>
                      <Item.Description>{item.home_price}</Item.Description>
                    </Item.Content>
                  </Item>
                )
              })}
            </Item.Group>
          </div>
        </div>
      )
    }

    // 解构赋值
    const {
      isShowCalc,
      isShowMap,
      swipers,
      menus,
      infos,
      faqs,
      houses
    } = this.state
    return (
      <div className="home-container">
        {isShowCalc && (
          <Calc
            hideCalc={() => {
              this.setState({ isShowCalc: false })
            }}
          />
        )}
        {isShowMap && (
          <Map
            hideMap={() => {
              this.setState({ isShowMap: false })
            }}
          />
        )}
        {/* 1.0 搜索框 */}
        <div className="home-topbar">
          <Input
            fluid
            icon={{ name: 'search', circular: true, link: true }}
            placeholder="搜房源..."
          />
        </div>
        {/* 2.0 加载器 */}
        <Dimmer inverted active={this.state.loading} page>
          <Loader>Loading</Loader>
        </Dimmer>
        {/* 3.0 内容区域 */}
        <div className="home-content">
          {/* 3.1 轮播图 */}
          <ImageGallery items={swipers} showThumbnails={false} />
          {/* 3.2 九宫格菜单 */}
          <Menus menus={menus} />
          {/* 3.3 咨询 */}
          <Infos infos={infos} />
          {/* 3.4 问答 */}
          <Faqs faqs={faqs} />
          {/* 3.5 房屋列表 */}
          <Houses houses={houses} />
        </div>
      </div>
    )
  }
}
