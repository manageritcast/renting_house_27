import React from 'react'
import { Icon } from 'semantic-ui-react'
import axios from 'axios'

export default class Map extends React.Component {
  constructor() {
    super()

    this.state = {
      // 地图上的点
      list: [
        {
          id: 1,
          x: '116.43244',
          y: '39.929986',
          type: 1
        },
        {
          id: 2,
          x: '116.424355',
          y: '39.92982',
          type: 1
        },
        {
          id: 3,
          x: '116.423349',
          y: '39.935214',
          type: 1
        },
        {
          id: 4,
          x: '116.350444',
          y: '39.931645',
          type: 1
        },
        {
          id: 5,
          x: '116.351684',
          y: '39.91867',
          type: 1
        },
        {
          id: 6,
          x: '116.353983',
          y: '39.913855',
          type: 1
        },
        {
          id: 7,
          x: '116.357253',
          y: '39.923152',
          type: 1
        },
        {
          id: 8,
          x: '116.349168',
          y: '39.923152',
          type: 1
        },
        {
          id: 9,
          x: '116.354954',
          y: '39.935767',
          type: 1
        },
        {
          id: 10,
          x: '116.36232',
          y: '39.938339',
          type: 1
        },
        {
          id: 11,
          x: '116.374249',
          y: '39.94625',
          type: 1
        },
        {
          id: 12,
          x: '116.380178',
          y: '39.953053',
          type: 1
        }
      ]
    }
  }

  initMap = () => {
    //百度地图的使用 http://lbsyun.baidu.com/index.php?title=jspopular
    const BMap = window.BMap
    // 创建地图实例
    const map = new BMap.Map('container')
    // 创建点坐标
    const point = new BMap.Point(116.404, 39.915)
    // 初始化地图，设置中心点坐标和地图级别
    map.centerAndZoom(point, 15)
    // 地图的鼠标滚轮缩放默认是关闭的，需要配置开启。
    map.enableScrollWheelZoom(true)
    // 添加比例尺
    map.addControl(new BMap.ScaleControl())
    // 添加平移缩放控件
    map.addControl(new BMap.NavigationControl())
    // 添加地图覆盖物
    this.state.list.forEach(item => {
      // 创建点
      var point = new BMap.Point(item.x, item.y)
      // 创建标注
      var marker = new BMap.Marker(point)
      // 将标注添加到地图中
      map.addOverlay(marker)
    })
  }

  /** 
  async componentWillMount() {
    const res = await axios.post('homes/map')

    this.initMap()
  }
  */
  componentDidMount() {
    this.initMap()
  }

  render() {
    return (
      <div className="map-house">
        <div className="map-house-title">
          <Icon onClick={this.props.hideMap} name="angle left" size="large" />
          地图找房
        </div>
        <div className="map-house-content" id="container" />
      </div>
    )
  }
}
