import React from 'react'
import { Icon, Tab, Grid, Dropdown, Input, Button } from 'semantic-ui-react'
import ReactEcharts from 'echarts-for-react'

class MyCharts extends React.Component {
  getOption = () => {
    const { chartData } = this.props

    return {
      title: {
        text: '贷款数据统计',
        // subtext: '纯属虚构',
        x: 'center'
      },
      tooltip: {
        trigger: 'item',
        formatter: '{c}'
        // formatter: "{a} <br/>{b} : {c} ({d}%)"
      },
      legend: {
        orient: 'vertical',
        left: 'left',
        data: ['贷款总额', '支付利息']
      },
      series: [
        {
          name: '访问来源',
          type: 'pie',
          radius: '55%',
          center: ['50%', '60%'],
          // data:[
          //   {value:335, name:'贷款总额'},
          //   {value:310, name:'支付利息'},
          //   {value:200, name:'利息'}
          // ],
          data: chartData,
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }
      ]
    }
  }

  render() {
    return <ReactEcharts option={this.getOption()} />
  }
}

class First extends React.Component {
  constructor() {
    super()

    this.state = {
      total: 0,
      // 默认选中的类型、年份、利率
      type: 1,
      year: 1,
      rate: 1,
      // 贷款类型
      types: [
        { key: 1, text: '按房间总额', value: 1 },
        { key: 2, text: '按贷款总额', value: 2 }
      ],
      // 贷款年限
      years: [
        { key: 1, text: '10年', value: 1 },
        { key: 2, text: '20年', value: 2 },
        { key: 3, text: '30年', value: 2 }
      ],
      // 贷款利率
      rates: [
        { key: 1, text: '基准利率(4.9%)', value: 1 },
        { key: 2, text: '基准利率9.5折', value: 2 },
        { key: 3, text: '基准利率9折', value: 3 },
        { key: 4, text: '基准利率8.5折', value: 4 }
      ],
      // echarts数据
      chartData: [
        { value: 335, name: '贷款总额' },
        { value: 310, name: '支付利息' }
      ]
    }
  }

  // 更改贷款类型、贷款年限、贷款利率
  changeType = (e, data) => {
    this.setState({ type: data.value })
  }
  changeYear = (e, data) => {
    this.setState({ year: data.value })
  }
  changeRate = (e, data) => {
    this.setState({ rate: data.value })
  }

  // 更改贷款总额
  changeTotal = (e, data) => {
    this.setState({ total: data.value })
  }

  // 重新计算贷款总金额和利率
  handleCalc = () => {
    this.setState({
      chartData: [
        { value: this.state.total, name: '贷款总额' },
        { value: parseInt(this.state.total) * 0.49, name: '利息' }
      ]
    })
  }

  render() {
    // 解构赋值
    const {
      total,
      type,
      year,
      rate,
      types,
      years,
      rates,
      chartData
    } = this.state
    return (
      <Grid columns={2}>
        <Grid.Row>
          <Grid.Column width={6}>贷款方式</Grid.Column>
          <Grid.Column width={10}>
            <Dropdown
              placeholder="请选择贷款方式"
              onChange={this.changeType}
              selection
              options={types}
              value={type}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={6}>贷款总额(万元)</Grid.Column>
          <Grid.Column width={10}>
            <Input
              value={total}
              onChange={this.changeTotal}
              className="calc-first-total"
              placeholder="请输入贷款总额"
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={6}>贷款年限</Grid.Column>
          <Grid.Column width={10}>
            <Dropdown
              placeholder="请选择贷款年限"
              onChange={this.changeYear}
              selection
              options={years}
              value={year}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={6}>贷款利率</Grid.Column>
          <Grid.Column width={10}>
            <Dropdown
              placeholder="请选择贷款利率"
              onChange={this.changeRate}
              selection
              options={rates}
              value={rate}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <Button onClick={this.handleCalc} fluid color="green">
              计算
            </Button>
          </Grid.Column>
        </Grid.Row>
        <div className="calc-chart">
          <MyCharts chartData={chartData} />
        </div>
      </Grid>
    )
  }
}

export default class List extends React.Component {
  render() {
    const panes = [
      {
        menuItem: '公积金贷款',
        render: () => (
          <Tab.Pane>
            <First />
          </Tab.Pane>
        )
      },
      { menuItem: '商业贷款', render: () => <Tab.Pane>商业贷款</Tab.Pane> },
      { menuItem: '组合贷款', render: () => <Tab.Pane>组合贷款</Tab.Pane> }
    ]

    const { hideCalc } = this.props
    return (
      <div className="home-calc">
        <div className="home-calc-title">
          <Icon onClick={hideCalc} name="angle left" size="large" />
          贷款利率计算
        </div>
        <Tab panes={panes} />
      </div>
    )
  }
}
