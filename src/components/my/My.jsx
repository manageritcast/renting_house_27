import React, { Component } from 'react'
import axios from 'axios'
import { Button, Grid, Icon, Modal } from 'semantic-ui-react'
import AvatarEditor from 'react-avatar-editor'
import { Toast } from 'antd-mobile'

import './My.css'

/**
 * 裁剪图片Modal
 */
class CropImageModal extends Component {
  constructor() {
    super()

    this.state = {
      scale: 1
    }

    this.scaleRef = React.createRef()
  }

  changeScale = e => {
    // console.log(e.target)
    // const value = parseFloat(this.scaleRef.current.value)

    // this.setState({
    //   scale:value
    // })

    this.setState({
      scale: parseFloat(e.target.value)
    })
  }

  // 上传图片
  upload = async () => {
    const base64Img = this.refs.AvatarEditorRef.getImageScaledToCanvas().toDataURL()

    // 发送请求给后台
    const res = await axios.post('my/avatar', {
      avatar: base64Img
    })

    if (res.meta.status === 200) {
      this.props.onCropImageModalClose(base64Img)
    }
  }

  render() {
    const { isOpenCropImageModal, onCropImageModalClose, avatar } = this.props
    return (
      <Modal
        size="small"
        open={isOpenCropImageModal}
        onClose={onCropImageModalClose}
      >
        <Modal.Header>上传头像</Modal.Header>
        <Modal.Content>
          <AvatarEditor
            ref="AvatarEditorRef"
            image={avatar}
            borderRadius={90}
            width={180}
            height={180}
            border={50}
            color={[255, 255, 255, 0.6]} // RGBA
            scale={this.state.scale}
            rotate={0}
          />
          <div>
            <span className="avatar-zoom">缩放:</span>
            <input
              ref={this.scaleRef}
              onChange={this.changeScale}
              step="0.01"
              min="1"
              max="2"
              defaultValue="1"
              type="range"
            />
          </div>
        </Modal.Content>
        <Modal.Actions>
          <Button
            positive
            icon="checkmark"
            onClick={this.upload}
            labelPosition="right"
            content="确定"
          />
        </Modal.Actions>
      </Modal>
    )
  }
}

/**
 * 选择图片Modal
 */
class SelectImageModal extends Component {
  constructor() {
    super()

    this.imgRef = React.createRef()
  }

  selectImg = () => {
    const file = this.imgRef.current.files[0]

    this.props.selectImage(file)
  }

  render() {
    const { isOpenImageModal, onImageModalClose } = this.props
    return (
      <Modal size="small" open={isOpenImageModal} onClose={onImageModalClose}>
        <Modal.Header>选择要裁剪的图片</Modal.Header>
        <Modal.Content>
          <input type="file" ref={this.imgRef} />
        </Modal.Content>
        <Modal.Actions>
          <Button
            onClick={this.selectImg}
            positive
            icon="checkmark"
            labelPosition="right"
            content="确定"
          />
        </Modal.Actions>
      </Modal>
    )
  }
}

export default class Info extends React.Component {
  constructor() {
    super()

    this.state = {
      uname: '',
      avatarPath: '',
      isOpenImageModal: false, //是否显示图片选择Modal
      isOpenCropImageModal: false, //是否显示裁剪图片Modal
      avatar: null //选择的头像
    }
  }
  async componentDidMount() {
    const res = await axios.post('my/info', {
      user_id: localStorage.getItem('uid')
    })

    this.setState({
      uname: res.data.username,
      avatarPath: res.data.avatar
    })
  }

  // 选择图片的Modal关闭
  onImageModalClose = () => {
    this.setState({
      isOpenImageModal: false
    })
  }

  // 裁剪图片的Modal关闭
  onCropImageModalClose = base64Img => {
    this.setState({
      isOpenCropImageModal: false, //关闭裁剪的对话框
      avatarPath: base64Img
    })
  }

  // 当选择了图片
  selectImage = file => {
    if (!file) {
      Toast.show('请选择图片!', 2)
      return
    }

    this.setState({
      avatar: file,
      isOpenImageModal: false,
      isOpenCropImageModal: true
    })
  }

  render() {
    const {
      uname,
      avatarPath,
      isOpenImageModal,
      isOpenCropImageModal,
      avatar
    } = this.state
    return (
      <div className="my-container">
        {/* 选择图片Modal */}
        <SelectImageModal
          isOpenImageModal={isOpenImageModal}
          onImageModalClose={this.onImageModalClose}
          selectImage={this.selectImage}
        />
        {/* 裁剪图片Modal */}
        <CropImageModal
          isOpenCropImageModal={isOpenCropImageModal}
          onCropImageModalClose={this.onCropImageModalClose}
          avatar={avatar}
        />
        <div className="my-title">
          {/* 背景 */}
          <img src="http://47.96.21.88:8086/public/my-bg.png" alt="" />
          {/* 个人信息 */}
          <div className="info">
            <div
              onClick={() => {
                this.setState({ isOpenImageModal: true })
              }}
              className="myicon"
            >
              <img src={avatarPath} alt="" />
            </div>
            <div className="name">{uname}</div>
            <Button color="green" size="mini">
              已认证
            </Button>
            <div className="edit">编辑个人资料</div>
          </div>
        </div>
        {/* 九宫格菜单 */}
        <Grid padded className="my-menu">
          <Grid.Row columns={3}>
            <Grid.Column>
              <Icon name="clock outline" size="big" />
              <div>看房记录</div>
            </Grid.Column>
            <Grid.Column>
              <Icon name="yen sign" size="big" />
              <div>我的订单</div>
            </Grid.Column>
            <Grid.Column>
              <Icon name="bookmark outline" size="big" />
              <div>我的收藏</div>
            </Grid.Column>
            <Grid.Column>
              <Icon name="user outline" size="big" />
              <div>个人资料</div>
            </Grid.Column>
            <Grid.Column>
              <Icon name="home" size="big" />
              <div>身份认证</div>
            </Grid.Column>
            <Grid.Column>
              <Icon name="microphone" size="big" />
              <div>联系我们</div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <div className="my-ad">
          <img src={'http://47.96.21.88:8086/public/ad.png'} alt="" />
        </div>
      </div>
    )
  }
}
