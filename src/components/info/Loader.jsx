import React from 'react'
import Tloader from 'react-touch-loader'
import { Item, Button, Icon, Modal, TextArea } from 'semantic-ui-react'
import axios from 'axios'
import './Tab.css'

export default class Loader extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      type: props.type, //类型 1:咨询 2:头条 3:问答
      // 0: do not display the progress bar
      // 1: start progress bar
      // 2: progress to end
      initializing: 0,
      hasMore: true, // 是否还有更多
      pagenum: 0, //开始条数
      pagesize: 2, //页容量
      listData: [], //数据列表
      open: false, //是否显示模态窗口
      comment: '' //评论内容
    }
  }

  componentWillMount() {
    // 加载数据
    this.loadData()
  }

  componentWillReceiveProps(nextProps) {
    this.setState(
      {
        type: nextProps.type,
        pagenum: 0,
        listData: []
      },
      () => {
        // 加载数据
        this.loadData()
      }
    )
  }

  // 加载数据
  async loadData(callback) {
    const { type, pagenum, pagesize, listData } = this.state
    console.log('加载type', type)

    const res = await axios.post('infos/list', {
      type,
      pagenum,
      pagesize
    })

    let newArray = listData.concat(res.data.list.data)
    const hasMore = newArray.length < res.data.list.total
    this.setState(
      {
        hasMore,
        listData: newArray
      },
      () => {
        callback && callback()
      }
    )
  }

  // 加载更多
  handleLoadMore = resolve => {
    this.setState(
      {
        pagenum: this.state.pagenum + this.state.pagesize
      },
      () => {
        this.loadData(resolve)
      }
    )
  }

  // 下拉刷新
  handleRefresh = (resolve, reject) => {
    this.setState(
      {
        listData: [],
        pagenum: 0,
        pagesize: 2
      },
      () => {
        this.loadData(resolve)
      }
    )
  }

  // 自动加载
  handleAutoLoadMore = () => {
    console.log('aaaaaaaa')
  }

  // 提交评论
  submitComment = async () => {
    if (this.state.comment.trim().length === 0) {
      alert('内容不能为空!')
      return
    }

    console.log(this.state.comment)
    const res = await axios.post('infos/question', {
      question: this.state.comment,
      question_time: new Date()
    })

    console.log(res)
    this.setState({
      open: false
    })
  }

  // 渲染列表页面
  initListView = () => {
    const { type, listData, open, comment } = this.state
    if (type === 1 || type === 2) {
      return (
        <Item.Group divided unstackable>
          {listData.map(item => {
            return (
              <Item key={item.id}>
                <Item.Image
                  size="tiny"
                  src="http://47.96.21.88:8086/public/1.png"
                />

                <Item.Content>
                  <Item.Header className="info-title">
                    {item.info_title}
                  </Item.Header>
                  <Item.Meta>$1200 1 Month</Item.Meta>
                </Item.Content>
              </Item>
            )
          })}
        </Item.Group>
      )
    } else {
      return (
        <div>
          {/* 1.0 模态窗口 */}
          <Modal size="small" open={open} onClose={this.close}>
            <Modal.Header>发表评论</Modal.Header>
            <Modal.Content>
              <TextArea
                value={comment}
                style={{ width: '100%', border: 0 }}
                rows={5}
                onChange={e => {
                  this.setState({ comment: e.target.value })
                }}
                placeholder="请输入评论内容..."
              />
            </Modal.Content>
            <Modal.Actions>
              <Button
                onClick={() => {
                  this.setState({ open: false })
                }}
                negative
              >
                取消
              </Button>
              <Button
                positive
                onClick={this.submitComment}
                icon="checkmark"
                labelPosition="right"
                content="发表"
              />
            </Modal.Actions>
          </Modal>
          {/* 2.0 提问部分 */}
          <div className="info-ask-btn">
            <Button
              onClick={() => {
                this.setState({ open: true })
              }}
              color="green"
              fluid
            >
              快速提问
            </Button>
          </div>
          {/* 问答列表部分 */}
          <ul className="info-ask-list">
            {listData.map((item, index) => {
              return (
                <li key={index}>
                  <div className="title">
                    <span className="cate">
                      <Icon color="green" name="users" size="small" />
                    </span>
                    <span>{item.question_name}</span>
                  </div>
                  {item.answer_content && (
                    <div className="user">
                      <Icon circular name="users" size="mini" />
                      {item.username} 的回答
                    </div>
                  )}
                  <div className="info">{item.answer_content}</div>
                  <div className="tag">
                    {item.question_tag &&
                      item.question_tag.split(',').map((tag, index) => {
                        return <span key={index}>{tag}</span>
                      })}
                    <span>{item.qnum ? item.qnum : 0}个回答</span>
                  </div>
                </li>
              )
            })}
          </ul>
        </div>
      )
    }
  }

  render() {
    const { initializing, hasMore } = this.state
    return (
      <div className="view">
        <Tloader
          initializing={initializing}
          onRefresh={this.handleRefresh}
          hasMore={hasMore}
          onLoadMore={this.handleLoadMore}
          autoLoadMore={this.handleAutoLoadMore}
          className="main"
        >
          {this.initListView()}
        </Tloader>
      </div>
    )
  }
}
