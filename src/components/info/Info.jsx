import React from 'react'
import { Tab } from 'semantic-ui-react'
import './Info.css'
import Loader from './Loader'

export default class Info extends React.Component {
  constructor() {
    super()

    this.state = {
      type: 1
    }
  }

  // tab改变了
  tabChange = (event, data) => {
    this.setState({
      type: data.activeIndex + 1
    })
  }

  render() {
    const { type } = this.state

    const panes = [
      {
        menuItem: '咨询',
        render: () => (
          <Tab.Pane>
            <Loader type={type} />
          </Tab.Pane>
        )
      },
      {
        menuItem: '头条',
        render: () => (
          <Tab.Pane>
            <Loader type={type} />
          </Tab.Pane>
        )
      },
      {
        menuItem: '回答',
        render: () => (
          <Tab.Pane>
            <Loader type={type} />
          </Tab.Pane>
        )
      }
    ]
    return (
      <div className="find-container">
        <div className="find-topbar">资讯</div>
        <div className="find-content">
          <Tab onTabChange={this.tabChange} panes={panes} />
        </div>
      </div>
    )
  }
}
